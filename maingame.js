// References: 
// http://www.iguanademos.com/Jare/docs/html5/Lessons/Lesson2/js/GameLoopManager.js
// https://www.html5rocks.com/en/tutorials/canvas/notearsgame/
// https://code.tutsplus.com/tutorials/animating-game-menus-and-screen-transitions-in-html5-a-guide-for-flash-developers--active-11183
// http://html5.litten.com/moving-shapes-on-the-html5-canvas-with-the-keyboard/
// https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_webstorage_local
// http://bencentra.com/code/2014/12/05/html5-canvas-touch-events.html

// http://bencentra.com/code/2014/12/05/html5-canvas-touch-events.html

// Global Variables
canvas = null;
context = null;
fpsElement = null;

menu = null;

var MainGame = function()
{
    canvas = document.getElementById('game-canvas'),
    context = canvas.getContext('2d'),
    fpsElement = document.getElementById('fps'),

    //Game states
    this.paused = false,
    this.PAUSED_CHECK_INTERVAL = 200, //time in milliseconds
    this.pauseStartTime,
    this.windowHasFocus = true,
    this.levelComplete = false,
    this.gameStarted = false,
    this.mainmenu = true,

    //Images
    this.background = new Image(),
    this.menu = new Image(),
    this.highlighter = new Image(),
    this.spritesheet = new Image(),
    this.player = new Image(),
    this.playerShot = new Image(),
    this.enemyShip = new Image(),
    this.enemySaucer = new Image(),
    this.lifeSprite = new Image(),
    
    // Animation 
    this.ANIMATION_RATE = 2,
    this.titleFlicker = true;
    this.flickers = 0;
            
    // Player Movement
    this.INITIAL_PLAYER_X = canvas.width/2-350;
    this.INITIAL_PLAYER_Y = canvas.height/2-60;
    this.playerX = this.INITIAL_PLAYER_X;
    this.playerY = this.INITIAL_PLAYER_Y;
    this.playerVelocity = 160;
    
    //Pixels and meters
    this.CANVAS_WIDTH_IN_METRES = 20,
    this.PIXELS_PER_METRE = canvas.width/this.CANVAS_WIDTH_IN_METRES;

    //Gravity
    this.GRAVITY_FORCE = 9.81;
    
    //Music
    this.onmainmenu = true;
    this.shotfiredsound = true;
    this.expsound = true;
    this.ongameover = false;
    
    // Scoring
    this.DEFAULT_SCORE = 0,
    this.score = 0;
    this.high_score = localStorage.getItem("highscore");
    this.lives = 3; 
    
    this.INVINCIBILITY = false;
    this.collisionTime;
    
    // Inputs
    this.touchX = null;
    this.touchY = null;
    
    // Player Ship Control Checks
    this.UP = false,
    this.DOWN = false,
    this.LEFT = false,
    this.RIGHT = false,
    
    this.shotFired = false,
    
    // Level
    this.STARTING_LEVEL = 1,
    this.level = this.STARTING_LEVEL;
    this.levelEndFlag = 9500,
            
    this.GAME_OVER = false;
            
    
    // Win Animation Variables
    this.winAnimationComplete = false;
    this.beginTakeOffAnimation = false;
    
    // Menu Variables
    this.menuMove = 1;
    this.menuSelect = 0;
    this.keyInput = 0;

    //Time
    this.lastAnimationFrameTime = 0,
    this.lastFpsUpdateTime = 0,
    this.fps = 60,

    //Background Scrolling
    this.BACKGROUND_OFFSET = 0,
    this.BACKGROUND_VELOCITY = 75,
    
    //Sprite Scrolling
    // Change to SpriteVelocity later
    this.STARTING_SPRITE_OFFSET = 80,
    this.spriteOffset = this.STARTING_SPRITE_OFFSET,
    this.spriteVelocity = this.spriteOffset;
    
    
    //Translation offsets
    this.backgroundOffset = this.BACKGROUND_OFFSET,
    this.backgroundVelocity = this.BACKGROUND_VELOCITY,
    
    // SPRITE
    this.enemyShips = [];
    this.enemySaucers = [];
    this.meteors = [];
    this.sprites = [];
    
    this.PLAYER_CELLS_WIDTH = 75,
    this.PLAYER_CELLS_HEIGHT = 100,
    
    this.playerCellsForward = 
    [
        // Normal
        { 
            left: 196, 
            top: 185, 
            width: this.PLAYER_CELLS_WIDTH, 
            height: this.PLAYER_CELLS_HEIGHT 
        }
    ],
            
    this.playerCellsRight =
    [
        // RIGHT
        {   
            left: 281, 
            top: 214, 
            width: this.PLAYER_CELLS_WIDTH, 
            height: this.PLAYER_CELLS_HEIGHT 
        }
    ],
    
    this.playerCellsLeft =
    [
        // Left
        { 
            left: 281, 
            top: 114, 
            width: this.PLAYER_CELLS_WIDTH, 
            height: this.PLAYER_CELLS_HEIGHT 
        }
    ],
    
    this.playerCellsDamage =
    [
    
        // Damaged
        { 
            left: 275, 
            top: 5, 
            width: this.PLAYER_CELLS_WIDTH, 
            height: this.PLAYER_CELLS_HEIGHT 
        }
    ],
    
    this.enemy1 =
    [
        {
            left: 5,
            top: 5,
            width: 50,
            height: 98,
        }
    ],
            
    this.enemy2 =
    [
        {
            left: 65,
            top: 5,
            width: 91,
            height: 91,
        }
    ],
            
    this.meteorLarge =
    [
        // Large
        {
            left: 50,
            top: 133,
            width: 136,
            height: 111,
        }
    ],
    
    this.meteorSmall =
    [
        // Small
        {
            left: 196,
            top: 133,
            width: 44,
            height: 42,
        }
    ],
            
    this.enemyShot =
    [
        {
            left: 166,
            top: 69,
            width: 33,
            height: 9,
        },
        
        {
            left: 209,
            top: 69,
            width: 56,
            height: 54,
        },
    ],
            
    this.playerShotCells =
    [
        {
            left: 165,
            top: 5,
            width: 33,
            height: 9,
        },
        
        // Hit
        {
            left: 209,
            top: 5,
            width: 56,
            height: 54,
        },
    ],
            
    this.life =
    [
        {
            left: 5,
            top: 133,
            width: 35,
            height: 27,
        }
    ];
    
    // Sprite Behaviours
    this.moveBehaviour = 
    {
        execute: function(sprite, now, fps, lastAnimationFrameTime)
        {
            if(game.started == false)
            {
                return;
            }
        }
    };
    
    this.collideBehaviour =
    {
        isCandidateForCollision: function(sprite, otherSprite)
        {
            var s, o;
            s = sprite.calculateSpriteHitBox()
            o = otherSprite.calculateSpriteHitBox();

            candidate =  o.left < s.right && sprite !== otherSprite &&
            sprite.visible && otherSprite.visible && !sprite.exploding && !otherSprite.exploding;
            return candidate;
        },
        
        didCollide: function(sprite, otherSprite)
        {
            var o = otherSprite.calculateSpriteHitBox(),
                r = sprite.calculateSpriteHitBox();
        
            context.beginPath();
            context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);
            var collision = context.isPointInPath(r.left, r.top) || context.isPointInPath(r.right, r.top) || context.isPointInPath(r.left, r.bottom) || context.isPointInPath(r.right, r.bottom) || context.isPointInPath(r.centreX, r.centreY);
            return collision;

        },
        
        processCollision: function(now)
        {
            // Player hit enemy
            if(game.INVINCIBILITY == false)
            {
                    game.playerHit(now); 
            }
        },
        
        execute: function(sprite, now, fps, lastAnimationFrameTime)
        {
            var otherSprite;
            if(game.INVINCIBILITY == false)
            {    
                for(var i=0; i < game.sprites.length; ++i)
                {
                    otherSprite = game.sprites[i];
                    if(otherSprite.type == "enemyShip" || otherSprite.type == "enemySaucer" || otherSprite.type == "meteor")
                    {
                        if(this.isCandidateForCollision(sprite, otherSprite))
                        {
                            if(this.didCollide(sprite, otherSprite))
                            {
                                this.processCollision(now);
                            }
                        }
                    }
                }
            }
        },
    };
    
    this.laserCollideBehaviour =
    {
        isCandidateForCollision: function(sprite, otherSprite)
        {
            var s, o;
            s = sprite.calculateLaserHitBox()
            o = otherSprite.calculateSpriteHitBox();

            candidate =  o.left < s.right && sprite !== otherSprite &&
            sprite.visible && otherSprite.visible && !sprite.exploding && !otherSprite.exploding;
            return candidate;
        },
        
        didCollide: function(sprite, otherSprite)
        {
            var o = otherSprite.calculateSpriteHitBox(),
                r = sprite.calculateLaserHitBox();
        
            context.beginPath();
            context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);
            var collision = context.isPointInPath(r.left, r.top) || context.isPointInPath(r.right, r.top) || context.isPointInPath(r.left, r.bottom) || context.isPointInPath(r.right, r.bottom) || context.isPointInPath(r.centreX, r.centreY);
            return collision;

        },
        
        processCollision: function(now, sprite, otherSprite, now)
        {
            // Laser hit enemy
            //sprite.artist.advance();
            game.playexpsound();
            game.resetShot();
            otherSprite.exploding = true;
            
        },
        
        execute: function(sprite, now, fps, lastAnimationFrameTime)
        {
            var otherSprite; 
            for(var i=0; i < game.sprites.length; ++i)
            {
                otherSprite = game.sprites[i];
                if(otherSprite.type == "enemyShip" || otherSprite.type == "enemySaucer" || otherSprite.type == "meteor")
                {
                    if(this.isCandidateForCollision(sprite, otherSprite))
                    {
                        if(this.didCollide(sprite, otherSprite))
                        {
                            this.processCollision(now, sprite, otherSprite, now);
                        }
                    }
                }
            }
         }
    };    
}

MainGame.prototype =
{
    initializeImages: function()
    {
       game.background.src = 'images/background.png';
       game.menu.src = 'images/Blank.png';
       game.highlighter.src = 'images/Right.png';
       this.spritesheet.src = 'images/spritesheet.png';
       
       this.background.onload = function(e)
        {
            game.startGame();
        };
    },
    
    // Fade in elements, may not be used,
    //backgroundLoaded: function()
    //{
        //var LOADING_SCREEN_TRANSITION_DURATION = 2000;
        //this.fadeOutElements(this.loadingElement, LOADING_SCREEN_TRANSITION_DURATION);
        //setTimeout(function(e){
                //game.startGame();
                //game.gameStarted = true;
        //}, LOADING_SCREEN_TRANSITION_DURATION);
    //},
    
    
    //MUSIC
    switchmusic: function()
    {
        if(this.onmainmenu === false)
        {
            var myAudio =document.getElementById("gamemusic");
            myAudio.src = "sounds/ingamemusic.mp3";
        }
        else
        {
            var myAudio =document.getElementById("gamemusic");
            myAudio.src = "sounds/gameovermusic.mp3";
        }
    },
    
    
    
    
    draw: function(now)
    {
        
        var menu = new MainMenu(game.menu, game.highlighter, game.menuMove, game.menuSelect);
        if(this.mainmenu == true)
        {
            menu.render(now);
            this.titleFlicker = now;   
        }
        else
        { 
            game.drawBackground();
            if(game.gameStarted == false)
            {
                if(this.onmainmenu === true && game.GAME_OVER === false)
                {
                    this.onmainmenu = false;
                    game.switchmusic();
                }
                game.drawLevelTitle(now);
                game.drawUI();
                game.drawSprites();
            }
            else
            {
                if(game.levelComplete == false)
                {
                    game.setOffsets(now);
                    //game.turnRight(now);
                    game.drawUI();
                    
                    
                    if(game.lives >= 0)
                    {
                        game.incrementScore();
                        game.checkHighScore();
                        game.updateSprites(now);
                        game.drawSprites();
                        game.checkPlayerMoveInputs(now);
                        game.levelCompleteCheck(now);
                        
                        if(game.INVINCIBILITY == true)
                        {
                            game.invincibility(now);
                        }
                        
                    }
                    else
                    {
                        if(game.onmainmenu == false)
                        {
                            game.onmainmenu = true;
                            game.switchmusic();
                        }
                        game.GAME_OVER = true;
                        game.gameOver();
                    }
                    
                }
                else
                {
                    game.drawUI();
                    game.updateSprites(now);
                    game.drawSprites();
                    game.levelCompleteAnimation(now);
                }
                //game.levelComplete == false;
            }
        }
    },
    
    drawBackground: function()
    {
        context.translate(-game.backgroundOffset, 0);
        context.drawImage(game.background, 0, 0);
        context.drawImage(game.background, game.background.width, 0);
        context.translate(game.backgroundOffset, 0);
    },
    
    startGame: function()
    {
        //this.togglePaused();
        //this.revealGame();
        //this.revealInitialToast();
        game.loadHighScore();
        window.requestAnimationFrame(game.animate);
    },
    
    animate: function(now)
    {
        if(game.paused)
        {
            setTimeout(function(){
                    requestAnimationFrame(game.animate);
            }, game.PAUSED_CHECK_INTERVAL);
        }
        else
        {
            fps = game.calculateFps(now);
            game.draw(now);
            game.lastAnimationFrameTime = now;
            requestAnimationFrame(game.animate);
        }
    },
    
    drawLevelTitle: function(now)
    {
        if(now - this.titleFlicker < 1600)
        {
            context.fillStyle = "White";
            context.font = "20px Century Gothic";
            context.textAlign = "center";
            context.fillText("Level " + game.level, canvas.width/2, canvas.height/2-50);
            context.fillStyle = "White";
            context.font = "40px Century Gothic";
            context.textAlign = "center";
            context.fillText("GET READY", canvas.width/2, canvas.height/2);
        }
        
        else if(now - this.titleFlicker < 2000)
        { 
            context.fillStyle = "White";
            context.font = "120px Century Gothic";
            context.textAlign = "center";
            context.fillText("GO", canvas.width/2, canvas.height/2+25);
        }
        
        else if(now - this.titleFlicker >= 2000)
        {
            game.gameStarted = true;
        }
    },
    
    setBackgroundOffset: function(now)
    {
        game.backgroundOffset += game.backgroundVelocity*(now - game.lastAnimationFrameTime)/1000;
        if(game.backgroundOffset < 0 || game.backgroundOffset > game.background.width)
        {
                game.backgroundOffset = 0;
        }
    },
    
    setOffsets: function(now)
    {
        game.setBackgroundOffset(now);
        game.setSpriteOffsets(now);
    },
    
    checkPlayerMoveInputs: function(now)
    {
        if(this.UP == true)
        {
            game.player.artist.cells = game.playerCellsLeft;
            game.movePlayerUp(now);
        }
        
        if(this.DOWN == true)
        {
            game.player.artist.cells = game.playerCellsRight;
            game.movePlayerDown(now);
        }
        
        if(this.LEFT == true)
        {
            game.movePlayerLeft(now);
        }
        
        if(this.RIGHT == true)
        {
            game.movePlayerRight(now);
        }
        
        if(this.shotFired == true)
        {
            game.fireShot(now);
        }
    },
    
    movePlayerUp: function(now)
    {
        this.playerY -= game.playerVelocity*(now - game.lastAnimationFrameTime)/1000;
        this.player.top = this.playerY;
        
        if(this.shotFired == false)
        {
            this.playerShot.top = this.player.top + 45;
        }
        
        
        if(this.playerY < 0)
        {
            this.playerY = 0;
            this.player.top = 0;
        }
    },
    
    movePlayerDown: function(now)
    {
        this.playerY += game.playerVelocity*(now - game.lastAnimationFrameTime)/1000;
        this.player.top = this.playerY;
        
        if(this.shotFired == false)
        {
            this.playerShot.top = this.player.top + 45;
        }
        
        
        if(this.playerY > canvas.height-125)
        {
            this.playerY = canvas.height-125;
            this.player.top = canvas.height-125;
        }
    },
    
    movePlayerLeft: function(now)
    {
        this.playerX -= game.playerVelocity*(now - game.lastAnimationFrameTime)/1000;
        this.player.left = this.playerX;
        
        if(this.shotFired == false)
        {
            this.playerShot.left = this.player.left + this.player.width;
        }
        
        if(this.playerX < 0)
        {
            this.playerX = 0;
            this.player.left = 0;
        }
    },
    
    movePlayerRight: function(now)
    {
        this.playerX += game.playerVelocity*(now - game.lastAnimationFrameTime)/1000;
        this.player.left = this.playerX;
        
        if(this.shotFired == false)
        {
            this.playerShot.left = this.player.left + this.player.width;
        }
        
        
        if(this.playerX > canvas.width-90)
        {
            this.playerX = canvas.width-90
            this.player.left = canvas.width-90;
        }
    },
    
    turnRight: function(now)
    {
        game.backgroundVelocity = game.BACKGROUND_VELOCITY;
    },
    
    drawUI: function()
    {
        context.fillStyle = "Black";
        context.fillRect(0, canvas.height-25, 800, 150);
        context.font = "20px Century Gothic";
        context.fillStyle = "White";
        context.textAlign = "left";
        if(!game.GAME_OVER)
        {
            context.fillText("       X" + this.lives, 0, canvas.height-5);
        }
        else
        {
            context.fillText("GAME OVER", 0, canvas.height-5);
        }
        context.textAlign = "center";
        context.fillText("HIGHSCORE: " + this.highscore, canvas.width/2, canvas.height-5);
        context.textAlign = "right";
        context.fillText("SCORE: " + this.score, canvas.width, canvas.height-5);
        
    },
    
    incrementScore: function()
    {
        game.score = game.score + 1;
    },
    
    loadHighScore: function()
    {
        if(this.high_score == "undefined")
        {
            localStorage.setItem('highscore', 1000)
            this.high_score = localStorage.getItem("highscore");
        }
        else
        {
            this.highscore = localStorage.getItem("highscore");
        }
    },
    
    checkHighScore: function()
    {
        if(game.score > this.highscore)
        {
            game.setHighScore();
        }
    },
    
    setHighScore: function()
    {
        this.highscore = game.score;
        localStorage.setItem('highscore', this.highscore);
    },
    
    resetBoard: function()
    {
        this.player.left = this.INITIAL_PLAYER_X;
	this.player.top = this.INITIAL_PLAYER_Y;
        this.playerX = this.INITIAL_PLAYER_X;
	this.playerY = this.INITIAL_PLAYER_Y;
        this.spriteOffset = this.STARTING_SPRITE_OFFSET;
    },
    
    playerHit: function(now)
    {
        this.resetBoard();
        this.collisionTime = now;
        this.lives = this.lives - 1;
        this.INVINCIBILITY = true;
    },
    
    enemyDestroyed: function(enemy, now) 
    {
        enemy.top += game.GRAVITY_FORCE * 
                    ((now - game.lastAnimationFrameTime)/1000) 
                    *game.CANVAS_WIDTH_IN_METRES;
    },
    
    playexpsound: function()
    {
        var myExp =document.getElementById("expsound");
        myExp.play();
    },
    
    
    // Invincibility Frames, lasts 5 seconds after taking hit.
    invincibility: function(now)
    {
        if(now - this.collisionTime < 7000)
        {
            return;
        }
        else
        {
            this.INVINCIBILITY = false;
        }
    },
    
    levelCompleteCheck: function(now)
    {
        if(game.levelEndFlag - game.spriteOffset  <= 0)
        {
            game.levelCompleteAnimation(now);
        }
    },
    
    levelCompleteProcess: function(now)
    {
        game.titleFlicker = now;
        game.level = game.level + 1;
        game.gameStarted = false;
        game.levelComplete = false;
        game.winAnimationComplete = false;
        game.resetBoard();
        // Fix this method later...
        game.increaseGameSpeed();
        game.addSpritesToSpriteArray();
        game.resetControls(); 
    },
    
    levelCompleteAnimation: function(now)
    {
        
        if(game.winAnimationComplete == false)
        {
            game.levelComplete = true;
            game.setPlayerShipWinAnimation(now);
            
        }
        else
        {
            
            game.levelCompleteProcess(now);
        }
    },
    
    drawWinTitle: function()
    {
        var titleLetters = ['L', ' ', 'E', ' ', 'V', ' ', 'E', ' ', 'L', ' ', ' ','C ', ' ', '  O ', ' ', 'M', ' ', 'P', ' ', 'L', ' ', 'E', ' ', 'T', ' ', 'E'];
        var add = 20;
        var distance = 120;
        context.font = "30px Century Gothic";
        for(var i = 0; i < titleLetters.length; i++)
        {
            if(game.player.left > distance + add)
            {
                context.fillText(titleLetters[i], distance + add, canvas.height/2);
                distance = distance + add;
            }
        }
        
        
    },
    
    setPlayerShipWinAnimation: function(now)
    {
        if(game.beginTakeOffAnimation == false)
            {
                if(game.player.left > 80)
                {
                    game.player.left -= 230*(now - game.lastAnimationFrameTime)/1000;
                }
                else
                {
                    game.beginTakeOffAnimation = true;
                }  
                
                if(game.player.top < canvas.height/2-60)
                {
                    game.player.top += 320*(now - game.lastAnimationFrameTime)/1000;
                }
                else if(game.player.top > canvas.height/2-60)
                {
                    game.player.top -= 320*(now - game.lastAnimationFrameTime)/1000;
                }
                
            }
            
            else if(game.beginTakeOffAnimation == true)
            {
                game.player.left += 460*(now - game.lastAnimationFrameTime)/1000;
                game.drawWinTitle();
                if(game.player.left > 1600)
                {
                    game.winAnimationComplete = true;
                }
            }
    },
    
    
    resetControls: function()
    {
        game.UP = false;
        game.DOWN = false;
        game.LEFT = false;
        game.RIGHT = false;
    },
    
    increaseGameSpeed: function()
    {
        game.backgroundVelocity = game.backgroundVelocity + (game.backgroundVelocity*0.30);
        game.spriteVelocity = game.spriteVelocity + (game.spriteVelocity *0.30);
        game.levelEndFlag = game.levelEndFlag + (game.levelEndFlag * 0.30);
    },

    calculateFps: function(now)
    {
        var fps = 1/(now - game.lastAnimationFrameTime) * 1000;
        if(now - game.lastFpsUpdateTime > 1000)
        {
                game.lastFpsUpdateTime = now;
                fpsElement.innerHTML = fps.toFixed(0) + ' fps'; 
        }
        if(fps < 30)
        {
            game.errorMessage(now);
        }
        return fps;
    },
    
    // Was unable to test this as I wasn't able to drop FPS low enough
    errorMessage: function(now)
    {
        game.paused == true;
        this.pauseStartTime = now;
        context.fillStyle = "Black";
        context.fillRect(0, canvas.height/2-75, 800, 150);
        context.stroke();
        context.fillStyle = "White";
        context.textAlign = "center";
        context.font = "50px Century Gothic";
        context.fillText("FPS IS VERY LOW. THIS CAN LEAD TO IRRATIC GAMEPLAY AND UNWANTED ISSUES", canvas.width/2, canvas.height/2);
        context.font = "20px Century Gothic";
        context.fillText("PRESS ENTER TO RESUME", canvas.width/2, canvas.height/2+25);    
    },
    
    togglePaused: function()
    {
        var now = +new Date();
        this.paused = !this.paused;
        if(this.paused)
        {
            this.pauseStartTime = now;
            context.fillStyle = "Black";
            context.fillRect(0, canvas.height/2-75, 800, 150);
            context.stroke();
            context.fillStyle = "White";
            context.textAlign = "center";
            context.font = "50px Century Gothic";
            context.fillText("PAUSED", canvas.width/2, canvas.height/2);
            context.font = "20px Century Gothic";
            context.fillText("PRESS ENTER TO RESUME", canvas.width/2, canvas.height/2+25);
        }
        else
        {
                this.lastAnimationFrameTime += (now - this.pauseStartTime);
        }
    }, 
    
    gameOver: function() //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    {
        context.fillStyle = "White";
        context.textAlign = "center";
        context.font = "50px Century Gothic";
        context.fillText("GAME OVER", canvas.width/2, canvas.height/2);
    },
    
    createPlayerSprite: function()
    {
        this.player = new Sprite('player', new SpriteSheetArtist(this.spritesheet, this.playerCellsForward), [this.moveBehaviour, this.collideBehaviour]);
        this.player.left = this.playerX;
	this.player.top = this.playerY;
    },
    
    createPlayerShotSprite: function()
    {
        this.playerShot = new Sprite('playerShot', new SpriteSheetArtist(this.spritesheet, this.playerShotCells), [this.laserCollideBehaviour]);
        this.playerShot.left = this.player.left + this.player.width;
        this.playerShot.top = this.player.top + 45;
        this.playerShot.visible = false;
    },
    
    createEnemyShips: function()
    {
        // Randomly generate how many enemy ships to spawn between 5 and 25
        var numberOfShips = Math.floor((Math.random() * 35) + 5);
        for(var i = 0; i < numberOfShips; i++)
        {
            this.enemyShip = new Sprite('enemyShip', new SpriteSheetArtist(this.spritesheet, this.enemy1), null);
            this.enemyShip.left = Math.floor((Math.random() * 8000) + 1000);
            this.enemyShip.top = Math.floor((Math.random() * 275) + 0);
            this.enemyShips.push(this.enemyShip);
        }
    },
    
    createEnemySaucers: function()
    {
        var numberOfShips = Math.floor((Math.random() * 35) + 5);
        for(var i = 0; i < numberOfShips; i++)
        {
            this.enemySaucer = new Sprite('enemySaucer', new SpriteSheetArtist(this.spritesheet, this.enemy2), null);
            this.enemySaucer.left = Math.floor((Math.random() * 8000) + 1000);
            this.enemySaucer.top = Math.floor((Math.random() * 275) + 0);
            this.enemySaucers.push(this.enemySaucer);
        }
    },
    
    createMeteors: function()
    {
        var numberOfLargeMeteors = Math.floor((Math.random() * 30) + 5);
        var numberOfSmallMeteors = Math.floor((Math.random() * 30) + 5);
        for(var i = 0; i < numberOfLargeMeteors; i++)
        {
            this.lMeteor = new Sprite('meteor', new SpriteSheetArtist(this.spritesheet, this.meteorLarge), null);
            this.lMeteor.left = Math.floor((Math.random() * 8000) + 1000);
            this.lMeteor.top = Math.floor((Math.random() * 275) + 0);
            this.meteors.push(this.lMeteor);
        }
        for(var i = 0; i < numberOfSmallMeteors; i++)
        {
            this.sMeteor = new Sprite('meteor', new SpriteSheetArtist(this.spritesheet, this.meteorSmall), null);
            this.sMeteor.left = Math.floor((Math.random() * 8000) + 1000);
            this.sMeteor.top = Math.floor((Math.random() * 275) + 0);
            this.meteors.push(this.sMeteor);
        }
    },
    
    createLifeCountSprite: function()
    {
        this.lifeSprite = new Sprite('life', new SpriteSheetArtist(this.spritesheet, this.life), null);
        this.lifeSprite.left = 0;
        this.lifeSprite.top = canvas.height-25;
        
    },
    
    fireShot: function(now)
    {
        this.playerShot.visible = true;
        this.playerShot.left += 500*(now - game.lastAnimationFrameTime)/1000;
        
        if(this.shotfiredsound === true)
        {
            this.fireshotsound();
            this.resetshotsound();
            
        }
        
        if(this.playerShot.left > canvas.width)
        {
            game.resetShot();
        }
    },
    
    resetshotsound: function()
    {
        this.shotfiredsound = false;   
    },
    
    fireshotsound: function()
    {
        var myShot =document.getElementById("shotsound");
        myShot.src = "sounds/shotfire.mp3";
        myShot.play();
        
    },
    
    resetShot: function()
    {
        this.shotFired = false;
        this.shotfiredsound = true;
        this.playerShot.visible = false;
        this.playerShot.left = this.player.left + this.player.width;
        this.playerShot.top = this.player.top + 45;
        
    },
    
    updateSprites: function(now)
    {
        var sprite;
        for(var i=0; i<this.sprites.length; i++)
        {
            sprite = this.sprites[i];
            if(sprite.visible && this.isSpriteInView(sprite))
            {
                context.translate(-sprite.hOffset, 0);
                sprite.update(now, this.fps, this.context, this.lastAnimationFrameTime);
                context.translate(sprite.hOffset, 0);
            }
        }
    },
    
    drawSprites: function()
    {
        var sprite;
        for(var i=0; i<this.sprites.length; i++)
        {
            //console.log(this.sprite[i].type);
            sprite = this.sprites[i];
            if(sprite.visible && this.isSpriteInView(sprite))
            {
                context.translate(-sprite.hOffset, 0);
                sprite.draw(context);
                context.translate(sprite.hOffset, 0);
            }
        }
    },

    isSpriteInView: function(sprite)
    {
        return sprite.left+sprite.width > sprite.hOffset && sprite.left < sprite.hOffset + canvas.width;
    },
    
    setSpriteOffsets: function (now) 
    {
        var sprite;
        // In step with platforms
        this.spriteOffset += this.spriteVelocity * (now - this.lastAnimationFrameTime) / 1000;

        // The reason it is length -2 is due to the player ship and life count icon being the final two sprites in the array
        // And they should not be affected by the sprite offset.
        for (var i=0; i < this.sprites.length-3; ++i) 
        {
            sprite = this.sprites[i];
            sprite.hOffset = this.spriteOffset;
            if(sprite.exploding == true)
            {
                game.enemyDestroyed(sprite, now)
            }
        }
    },
    
    createSprites: function()
    {
        this.createEnemyShips();
        this.createEnemySaucers();
        this.createMeteors()
        this.createPlayerSprite();
        this.createPlayerShotSprite();
        this.createLifeCountSprite();
        // Need to add to this later...
        //Add all of the sprites to a single array
        this.addSpritesToSpriteArray();
    },
    
    addSpritesToSpriteArray: function()
    {
        // Empty array if needs be...
        this.sprites = [];
        
        if(game.level % 3 == 0)
        {
            for(var i =0; i < this.meteors.length; ++i)
            {
                this.sprites.push(this.meteors[i]);
            }
        }
        else if(game.level % 2 == 0 )
        {
            for(var i =0; i < this.enemySaucers.length; ++i)
            {
                this.sprites.push(this.enemySaucers[i]);
            }
        }
        else
        {
            for(var i =0; i < this.enemyShips.length; ++i)
            {
                this.sprites.push(this.enemyShips[i]);
            }
        }
        this.sprites.push(this.player);
        this.sprites.push(this.playerShot);
        this.sprites.push(this.lifeSprite);
    },
    
    detectMobile: function () {
      game.mobile = 'ontouchstart' in window;
   },
   
    fitScreen: function () {
      var arenaSize = game.calculateArenaSize(
                         game.getViewportSize());

      game.resizeElementsToFitScreen(arenaSize.width, 
                                          arenaSize.height);
   },
   
    calculateArenaSize: function (viewportSize) {
      var DESKTOP_ARENA_WIDTH  = 800,  // Pixels
          DESKTOP_ARENA_HEIGHT = 520,  // Pixels
          arenaHeight,
          arenaWidth;

      arenaHeight = viewportSize.width * 
                    (DESKTOP_ARENA_HEIGHT / DESKTOP_ARENA_WIDTH);

      if (arenaHeight < viewportSize.height) { // Height fits
         arenaWidth = viewportSize.width;      // Set width
      }
      else {                                   // Height does not fit
         arenaHeight = viewportSize.height;    // Recalculate height
         arenaWidth  = arenaHeight *           // Set width
                      (DESKTOP_ARENA_WIDTH / DESKTOP_ARENA_HEIGHT);
      }

      if (arenaWidth > DESKTOP_ARENA_WIDTH) {  // Too wide
         arenaWidth = DESKTOP_ARENA_WIDTH;     // Limit width
      } 

      if (arenaHeight > DESKTOP_ARENA_HEIGHT) { // Too tall
         arenaHeight = DESKTOP_ARENA_HEIGHT;    // Limit height
      }

      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },
   
   getViewportSize: function () {
      return { 
        width: Math.max(document.documentElement.clientWidth ||
               window.innerWidth || 0),  
               
        height: Math.max(document.documentElement.clientHeight ||
                window.innerHeight || 0)
      };
   },
   
   resizeElement: function (element, w, h) {
      console.log(element);
      element.style.width  = w + 'px';
      element.style.height = h + 'px';
   },

   resizeElementsToFitScreen: function (arenaWidth, arenaHeight) {
      game.resizeElement(
         document.getElementById('arena'), 
         arenaWidth, arenaHeight);
   },
}


var game = new MainGame();

window.addEventListener('keydown', function(e)
{
    if(game.mainmenu == true)
    {
        if(e.keyCode == 38 || e.keyCode == 87)
        {
            game.menuMove = 38;
            if(game.menuSelect > 0)
            {
                game.menuSelect--;
            }
        }

        if(e.keyCode == 40 || e.keyCode == 83)
        {
            game.menuMove = 40;
            if(game.menuSelect < 2)
            {
                game.menuSelect++;
            }
        }

        if(e.keyCode == 13 && game.menuSelect == 0)
        {
           game.mainmenu = false;
        }
    }
    
    if(game.paused && game.windowHasFocus)
    {
        if(e.keyCode == 13)
        {
            game.togglePaused();
        }
    }
    
    if(game.gameStarted && !game.paused)
    {
        if(e.keyCode == 38 || e.keyCode == 87)
        { 
            game.player.artist.cells = game.playerCellsLeft;
            game.UP = true;
        }

        if(e.keyCode == 40 || e.keyCode == 83)
        {
            game.player.artist.cells = game.playerCellsRight;
            game.DOWN = true;
        }
        
        if(e.keyCode == 37 || e.keyCode == 65)
        {
            game.LEFT = true;
        }
        
        if(e.keyCode == 39 || e.keyCode == 68)
        {
            game.RIGHT = true;
        }
        
        if(e.keyCode == 32)
        {
            game.shotFired = true;
        }
    }   
});

window.addEventListener('keyup', function(e)
{
    if(game.gameStarted && !game.paused)
    {
        if(e.keyCode == 37 || e.keyCode == 65)
        {
            game.LEFT = false;
        }
        
        else if(e.keyCode == 38 || e.keyCode == 87)
        {
            game.UP = false;
            game.player.artist.cells = game.playerCellsForward;
        }
        
        else if(e.keyCode == 39 || e.keyCode == 68)
        {
            game.RIGHT = false;   
        }
        
        else if(e.keyCode == 40 || e.keyCode == 83)
        {
            game.DOWN = false;
            game.player.artist.cells = game.playerCellsForward;
        }
    }  
    
});

window.addEventListener('blur', function(e)
{
    game.windowHasFocus = false;
    if(!game.paused && !game.mainmenu && !game.GAME_OVER)
    {
            game.togglePaused(); //Pause the game
}});

window.addEventListener('focus', function(e)
{
    game.windowHasFocus = true;
});


 
game.initializeImages();
game.createSprites();

game.detectMobile();

if (game.mobile) 
(
    document.body.addEventListener("touchstart", function (e) 
    {
        if(e.target == canvas && game.menuSelect == 0 && game.mainmenu == true)
        {
            game.mainmenu = false;
            e.preventDefault();
        }
        else if (e.target == canvas && game.gameStarted && !game.paused) 
        {
            game.shotFired = true;
            e.preventDefault();
        }
    })
 );
 
 if(game.mobile)
 {
    document.body.addEventListener("touchmove", function (e) 
    {
        if(game.touchX !== null && game.touchY !== null)
        {
            if(game.mainmenu == true)
            {
                if (e.target == canvas && e.touches[0].pageY > game.touchY) 
                {
                    game.menuMove = 38;
                    if(game.menuSelect > 0)
                    {
                        game.menuSelect--;
                    }
                }
                if (e.target == canvas && e.touches[0].pageY < game.touchY) 
                {
                    game.menuMove = 40;
                    if(game.menuSelect < 2)
                    {
                        game.menuSelect++;
                    }   
                }
            }
            
            else if(game.gameStarted && !game.paused)
            {
                if (e.target == canvas && e.touches[0].pageY > game.touchY) 
                {
                    game.DOWN = true;
                    game.player.artist.cells = game.playerCellsRight;
                    e.preventDefault();
                }
                else
                {
                    game.DOWN = false;
                }

                if (e.target == canvas && e.touches[0].pageY < game.touchY) 
                {
                    game.UP = true;
                    game.player.artist.cells = game.playerCellsLeft;
                    e.preventDefault();
                }
                else
                {
                    game.UP = false;
                }

                if (e.target == canvas && e.touches[0].pageX > game.touchX) 
                {
                    game.RIGHT = true;
                    e.preventDefault();
                }
                else
                {
                    game.RIGHT = false;
                }

                if (e.target == canvas && e.touches[0].pageX < game.touchX) 
                {
                    game.LEFT = true;
                    e.preventDefault();
                }
                else
                {
                    game.LEFT = false;
                }
            }
        }
        game.touchX = e.touches[0].pageX;
        game.touchY = e.touches[0].pageY;
    }, false);
 };
 
if(game.mobile)
{
   document.body.addEventListener("touchend", function (e) 
   {
       game.LEFT = false;
       game.RIGHT = false;
       game.UP = false;
       game.DOWN = false;
       game.touchX = null;
       game.touchY = null;
       e.preventDefault();
   }
)};
 

game.fitScreen();
window.addEventListener("resize", game.fitScreen);
window.addEventListener("orientationchange", game.fitScreen);