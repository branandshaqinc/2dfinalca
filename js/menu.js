
var MainMenu = function(background, highlighter, input, select)
{
    this.TITLE = "Goodbye Moonmen",
    this.ITEMS = ["Play", "Options", "Credits"],
    this.COPY = "Copyright Brandon Regan & Shaquille Mc Keown 2017",
    this.BACKGROUND = background;
    this.HIGHLIGHTER = highlighter;
    this.SELECTED = select;
    
    // Game States
    this.credits = false,
    this.options = false,
    
    // Inputs
    this.keyInput = input;
    this.mouseX = 0,
    this.mouseY = 0,
    
    //Scrolling
    this.STARTING_BACKGROUND_OFFSET = 0,
    this.STARTING_BACKGROUND_VELOCITY = 0,
    this.BACKGROUND_VELOCITY = 15,
    
    //Translation offsets
    this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
    this.backgroundVelocity = this.BACKGROUND_VELOCITY,
    
    this.lastAnimationFrameTime = 0;
}

MainMenu.prototype =
{
    render: function(now)
    {
        this.setOffsets(now);
        this.drawBackground();
        this.turnRight();
        this.drawTitles();
        
        
    },
    
    drawBackground: function()
    {
        context.translate(-this.backgroundOffset, 0);
        context.drawImage(this.BACKGROUND, 0, 0);
        context.drawImage(this.BACKGROUND, this.BACKGROUND.width, 0);
        context.translate(this.backgroundOffset, 0);
    },

    drawTitles: function()
    { 
        if (this.TITLE)
	{
            context.fillStyle = "White";
            context.textAlign = "left";
            context.font = "50px Century Gothic";
            context.fillText(this.TITLE, 75, 100);
            
	}
        
        var space = 100;
	for (var i = 0; i < this.ITEMS.length; ++i)
	{
            if(i == this.SELECTED)
            {
                context.textAlign = "left";
                
                this.drawHighlighter(space);
                if(i == 1)
                {
                    this.drawOptions();
                }
                if(i == 2)
                {
                    this.drawCredits();
                }
                context.font = "50px Century Gothic";
                space += 75;
                context.fillText(this.ITEMS[i], 140, space);
            }
            else
            {
                context.textAlign = "left";
                context.font = "30px Century Gothic";
                space += 75;
                context.fillText(this.ITEMS[i], 75, space);
            }
	}
	if (this.COPY)
	{
		context.textAlign = "right";
		context.font = "14px Times New Roman";
		context.fillText(this.COPY, canvas.width-1, canvas.height-3);
	}
    },
    
    setBackgroundOffset: function(now)
    {
        
        this.backgroundOffset += this.backgroundVelocity*(now - this.lastAnimationFrameTime)/1000;
        // Not working for some reason
        if(this.backgroundOffset < 0 || this.backgroundOffset > this.BACKGROUND.width)
        {
                this.backgroundOffset = 0;
        }
    },
    
    drawOptions: function()
    {

        context.font = "35px Century Gothic";
        context.fillText("USE THE ARROW KEYS", 400, 175);
    },
    
    drawCredits: function()
    {
        context.font = "15px Century Gothic";
        context.fillText("Game Developed By Brandon Regan & Shaquille Mc Keown", 350, 175);
    },
    
    setOffsets: function(now)
    {
        this.setBackgroundOffset(now);
    },
    
    turnRight: function()
    {
        this.backgroundVelocity = this.BACKGROUND_VELOCITY;
    },
    
    drawHighlighter: function(space)
    {
        this.HIGHLIGHTER.width = 20;
        context.drawImage(this.HIGHLIGHTER, 75, space+32);
    },
    
}
